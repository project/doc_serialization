<?php

namespace Drupal\doc_serialization\Encoder;

use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Drupal\file\Entity\File;
use \Drupal\Core\Utility\Error;

/**
 * Adds DOCX encoder support for the Serialization API.
 */
class Docx implements EncoderInterface {

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static $format = 'docx';

  /**
   * Format to write DOC files as.
   *
   * @var string
   */
  protected $docFormat = 'Word2007';

  /**
   * Constructs an DOCX encoder.
   *
   * @param string $doc_format
   *   The DOC format to use.
   */
  public function __construct($doc_format = 'Word2007') {
    $this->docFormat = $doc_format;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Serialization\Exception\InvalidDataTypeException
   */
  public function encode(mixed $data, $format, array $context = []): string {
    switch (gettype($data)) {
      case 'array':
        // Nothing to do.
        break;

      case 'object':
        $data = (array) $data;
        break;

      default:
        $data = [$data];
        break;
    }

    $views_style_plugin = $context['views_style_plugin'];
    $displayHandler = $views_style_plugin->displayHandler;

    try {
      $options = $displayHandler->display['display_options']['display_extenders']['doc_serialization']['doc_serialization'];
      if (!empty($options['file_path'])) {
        $file_path = realpath($options['file_path']);
      }
      else {
        $template_fid = $options['template_file'][0];
        $file = File::load($template_fid);
        $file_path = \Drupal::service('file_system')->realpath($file->getFileUri());
      }
      if (!$file_path) {
        throw new \LogicException('Doc serialization template file does not exist.');
      }

      $templateProcessor = new TemplateProcessor($file_path);

      $view = $views_style_plugin->view;
      $replacements = [];
      foreach ($data as $key => $row) {
        $fields = [];
        foreach ($row as $field => $value) {
          $fields[$field] = strip_tags($value);

          // Add label replacement in the form of `field.label`.
          $fields["${field}.label"] = $view->field[$field]->options['label'];
        }
        array_push($replacements, $fields);
      }
      $templateProcessor->cloneBlock('views_row', 0, true, false, $replacements);

      ob_start();
      $templateProcessor->saveAs('php://output');
      return ob_get_clean();
    }
    catch (\Exception $e) {
	$logger = \Drupal::logger('doc_serialization');
	Error::logException($logger, $e->getMessage());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format): bool {
    return $format === static::$format;
  }

}
